.PHONY: create-dev
create-dev: deps
	$(DOCKER_COMPOSE) run -e RAILS_ENV=development \
		spring /scripts/entrypoint/gitlab-rails-exec.sh /scripts/helpers/create-dev-env.sh

.PHONY: create-test
create-test: deps
	$(DOCKER_COMPOSE) run -e RAILS_ENV=test \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bash -c 'bin/rake -t db:drop db:prepare'

.PHONY: create-runner
create-runner: deps
	$(DOCKER_COMPOSE) run -e RAILS_ENV=development \
		spring bin/rails runner "Ci::Runner.create(runner_type: :instance_type, token: 'SHARED_RUNNER_TOKEN')"

.PHONY: create
create: create-dev create-test create-runner

.PHONY: migrate-dev
migrate-dev:
	$(DOCKER_COMPOSE) run -e RAILS_ENV=development \
		spring bash -c 'bin/rake db:migrate'

.PHONY: migrate-test
migrate-test:
	$(DOCKER_COMPOSE) run -e RAILS_ENV=test \
		spring bash -c 'bin/rake db:migrate'

.PHONY: update-dev
update-dev: update-repos
	make migrate-dev

.PHONY: update-test
update-test: update-repos
	make migrate-test

.PHONY: update
update: update-dev update-test

.PHONY: assets-compile
assets-compile:
	$(DOCKER_COMPOSE) run -e RAILS_ENV=test \
		spring bash -c 'bin/rake gitlab:assets:compile'

.PHONY: webpack-compile
webpack-compile:
	$(DOCKER_COMPOSE) run -e FORCE_WEBPACK_COMPILE=true webpack

.PHONY: env
env:
	./scripts/proxy bash

.PHONY: ports
ports:
	./scripts/proxy ./scripts/ports
